# -*- mode: makefile-gmake; coding: utf-8 -*-

# libs goes to: /usr/mips-linux-gnu/lib
# headers goes to: /usr/mips-linux-gnu/include
#For imx6 ln is "arm-openwrt-linux-as" and the file is "arm-openwrt-linux-muslgnueabi"

DESTDIR		=.

DEST		= imx6-toolchain
CONFIG		= config

OPENWRT_REPO	= git://github.com/openwrt/openwrt.git
OPENWRT_DIR	= openwrt
OPENWRT_SCRIPTS	= $(OPENWRT_DIR)/scripts

SOURCE_URL	= https://downloads.openwrt.org/snapshots/trunk/imx6/generic/
SOURCE_FILE	= OpenWrt-Toolchain-imx6_gcc-5.3.0_musl-1.1.15_eabi.Linux-x86_64.tar.bz2
SOURCE_DIR	= OpenWrt-Toolchain-imx6_gcc-5.3.0_musl-1.1.15_eabi.Linux-x86_64

# TCNAME		= toolchain-mips_34kc_gcc-4.8-linaro_uClibc-0.9.33.2
TCNAME		= toolchain-arm_cortex-a9+neon_gcc-5.3.0_musl-1.1.15_eabi
UCNAME		= target-arm_cortex-a9+neon_musl-1.1.15_eabi
INSTALL_DIR	= $(DEST)-install/pkg
STAGING_DIR	= $(DEST)/staging_dir
TOOLCHAIN_DIR	= $(DEST)/$(TCNAME)

TCDIR		= $(INSTALL_DIR)/staging_dir/$(TCNAME)
UCDIR		= $(INSTALL_DIR)/staging_dir/$(UCNAME)

INCLUDE		= /usr/include/openwrt-imx6-toolchain


# dh_make will call all
all:

	# make build-toolchain

clone:
	[-d $(DEST) ] || git clone $(OPENWRT_REPO) $(DEST)

download:

	wget $(SOURCE_URL)$(SOURCE_FILE)
	chmod +x $(SOURCE_FILE)
	tar -xf $(SOURCE_FILE)
	mv $(SOURCE_DIR) $(DEST)




# probably tools/install and toolchain/install are not
# needed because it have been installed before. TEST IT!!

build-toolchain:
	cp $(CONFIG)/imx6_config $(DEST)/.config
	make -C $(DEST) -j3 \
		tools/install \
		toolchain/install \
		package/libs/toolchain/install \
		package/libs/uclibc++/install \
		package/system/opkg/install

# dh_make will call install after call all
# comment lines are intended to be replace by correct sentences.
install:
	install -d $(DESTDIR)$(INCLUDE)

	install -v -m 444 make/imx6.mk $(DESTDIR)$(INLCUDE)

	install -vd $(DESTDIR)/usr/bin
	install -Dv $(TOOLCHAIN_DIR)/bin/* $(DESTDIR)/usr/bin

	# install -vd $(INCLUDE)
	# install -m 644 $(TOOLCHAIN_DIR)/include/*.h $(INCLUDE)

	# install -vd $(INCLUDE)/arpa
	# install -D $(TOOLCHAIN_DIR)/include/arpa* $(INCLUDE)/arpa

	# install -vd $(INCLUDE)/asm
	# install -vd $(INCLUDE)/asm-generic
	# install -vd $(INCLUDE)/bits
	# install -vd $(INCLUDE)/drm
	# install -vd $(INCLUDE)/fortify
	# install -vd $(INCLUDE)/linux
	# install -vd $(INCLUDE)/misc
	# install -vd $(INCLUDE)/mtd
	# install -vd $(INCLUDE)/net
	# install -vd $(INCLUDE)/netinet
	# install -vd $(INCLUDE)/netpacket
	# install -vd $(INCLUDE)/rdma
	# install -vd $(INCLUDE)/scsi
	# install -vd $(INCLUDE)/sound
	# install -vd $(INCLUDE)/sys
	# install -vd $(INCLUDE)/uapi
	# install -vd $(INCLUDE)/video
	# install -vd $(INCLUDE)/xen




	# install -d $(INSTALL_DIR)
        # # cp -r debian.in $(INSTALL_DIR)/debian

	# install -d $(TCDIR)
        # # cp -r $(TOOLCHAIN_DIR)/bin $(TCDIR)
        # # cp -r $(TOOLCHAIN_DIR)/include $(TCDIR)
        # # cp -r $(TOOLCHAIN_DIR)/lib $(TCDIR)
        # # cp -r $(TOOLCHAIN_DIR)/libexec $(TCDIR)
        # # rm $(TCDIR)/bin/ldd

	# ln -fs arm-openwrt-linux-muslgnueabi-as $(TCDIR)/bin/as

	# install -d $(TCDIR)/arm-openwrt-linux
	# # cp -r $(TOOLCHAIN_DIR)/mips-openwrt-linux/include $(TCDIR)/mips-openwrt-linux



	# install -d $(UCDIR)/usr
        # cp -r $(STAGING_DIR)/$(UCNAME)/usr/lib $(UCDIR)/usr


s_clean:
	$(RM) $(shell find -name *~)
	$(RM) -rf $(DEST)-install.tar.gz
	$(RM) -rf *~ $(DEST)
	$(RM) -rf OpenWrt-*
