# -*- mode: makefile-gmake; coding: utf-8 -*-

#libs goes to /usr.../lib
#headers in: /usr.../include


STAGING_DIR	= /usr/share/imx6-toolchain
TOOLCHAIN	= $(STAGING_DIR)/toolchain-arm_cortex-a9+neon_gcc-5.3.0_musl-1.1.15_eabi
GCCINCLUDE	= $(TOOLCHAIN)/arm-openwrt-linux/include/c++/5.3.0/
PATH		:= $(TOOLCHAIN)/bin:$(PATH)


export PATH
export STAGING_DIR


LDFLAGS		= -L(TOOLCHAIN_DIR)/lib \
		-L$(STAGING_DIR)/target-arm_cortex-a9+neon_musl-1.1.15_eabi/usr/lib \
		-L/usr/arm-openwrt-linux/lib

CXX		= arm-openwrt-linux-muslgnueabi-c++

CXXFLAGS	= --sysroot=$(TOOLCHAIN) -I. \
		-I$(TOOLCHAIN)/include \
		-I/usr/...../include \
		-I$(GCCINCLUDE) \
		-I$(GCCINCLUDE)/mips-openwrt-linux-uclib

STRIP		= arm-openwrt-linux-muslgnueabi-strip
